﻿using UnityEngine;

public class Player : MonoBehaviour
{
    // The force which is added when the player jumps
    // This can be changed in the Inspector window
    public Vector2 jumpForce = new Vector2(0, 300);
    public AudioSource jump;
    

    // Update is called once per frame
    void Update()
    {
        // Jump
        if (Input.GetKeyUp("space"))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(jumpForce);
            jump.Play();
        }

       
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Lose();
    }

    void Lose()
    {
		Application.LoadLevel(Application.loadedLevel);
    }
}