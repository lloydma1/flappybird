﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    float speed = 3;

    void Update()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
    }
}