﻿using UnityEngine;

public class Generate : MonoBehaviour
{
    public GameObject obstacle;
    public Vector3 position;
    public Quaternion rotation;

    public void Start()
    {
        InvokeRepeating("Go", 2f, 2f);
    }
    void Go()
    { 
        GameObject.Instantiate(obstacle, position, rotation);
    }

    
}